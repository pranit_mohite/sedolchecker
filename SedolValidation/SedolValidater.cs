﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SedolValidation
{
    class SedolValidater : ISedolValidator
    {
        public ISedolValidationResult ValidateSedol(string input)
        {
            var data = new sedolInfo();
            char[] alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            // Check if given string is number
            int n;
            bool isNumeric = int.TryParse(input, out n);
            // Check end
            var val1 = string.Empty; var val2 = string.Empty; var val3 = string.Empty; var val4 = string.Empty; var val5 = string.Empty; var val6 = string.Empty; var val7 = string.Empty;
            //int val1 = 0; int val2 = 0; int val3 = 0; int val4 = 0; int val5 = 0; int val6 = 0; int val7 = 0;
            bool isUserdefined = false;

            if (input == null || input == "" || input.Length > 7 || input.Length < 7)
            {
                data.InputString = input;
                data.IsValidSedol = false;
                data.IsUserDefined = false;
                data.ValidationDetails = "Input string was not 7-characters long";
            }
            if (!(input.All(c => Char.IsLetterOrDigit(c))))
            {
                data.InputString = input;
                data.IsValidSedol = false;
                data.IsUserDefined = false;
                data.ValidationDetails = "SEDOL contains invalid characters";
            }

            else if (input != null && input != "" && input.Length == 7)
            {
                // Check for valid sedol

                //var intList = input.Select(digit => int.Parse(digit.ToString())).ToList();
                var res = input.Select(x => new string(x, 1)).ToArray();
                if (res.Any())
                {
                    int counter = 0;
                    int counter2 = 0;
                    int weight = 0;
                    foreach (var i in res)
                    {
                        counter++;
                        int num;
                        bool isNumber = int.TryParse(i, out num);
                        if (!isNumber)
                        {
                            foreach (var j in alpha)
                            {
                                counter2++;
                                if (i == j.ToString())
                                {

                                    weight = counter2 + 9;
                                }


                            }
                        }
                        if (counter == 1)
                        {
                            if (isNumber == true)
                            {
                                val1 = Convert.ToString(Convert.ToInt32(i) * 1);
                            }

                            else
                            {
                                val1 = Convert.ToString(Convert.ToInt32(weight) * 1);

                            }
                            if (i == "9")
                            {
                                isUserdefined = true;
                            }
                            else
                            {
                                isUserdefined = false;

                            }

                        }

                        if (counter == 2)
                        {
                            if (isNumber == true)
                            {
                                val2 = Convert.ToString(Convert.ToInt32(i) * 3);
                            }

                            else
                            {
                                val2 = Convert.ToString(weight * 3);

                            }
                          
                        }

                        if (counter == 3)
                        {
                            if (isNumber == true)
                            {
                                val3 = Convert.ToString(Convert.ToInt32(i) * 1);
                            }

                            else
                            {
                                val3 = Convert.ToString(weight * 1);

                            }
                            
                        }

                        if (counter == 4)
                        {
                            if (isNumber == true)
                            {
                                val4 = Convert.ToString(Convert.ToInt32(i) * 7);
                            }

                            else
                            {
                                val4 = Convert.ToString(weight * 7);

                            }
                            
                        }
                        if (counter == 5)
                        {
                            if (isNumber == true)
                            {
                                val5 = Convert.ToString(Convert.ToInt32(i) * 3);
                            }

                            else
                            {
                                val5 = Convert.ToString(weight * 3);

                            }
                            
                        }
                        if (counter == 6)
                        {
                            if (isNumber == true)
                            {
                                val6 = Convert.ToString(Convert.ToInt32(i) * 9);
                            }

                            else
                            {
                                val6 = Convert.ToString(weight * 9);

                            }

                        }
                        if (counter == 7)
                        {
                            val7 = i;

                        }


                    }
                    // Summing up the results
                    var dataresult = Convert.ToInt32(val1) + Convert.ToInt32(val2) + Convert.ToInt32(val3) + Convert.ToInt32(val4) + Convert.ToInt32(val5) + Convert.ToInt32(val6);
                    var checksum = (10 - (dataresult % 10)) % 10;
                    if (checksum == Convert.ToInt32(val7))
                    {
                        data.InputString = input;
                        data.IsValidSedol = true;
                        data.IsUserDefined = isUserdefined;
                        data.ValidationDetails = null;

                    }

                    else if (checksum != Convert.ToInt32(val7))
                    {
                        data.InputString = input;
                        data.IsValidSedol = false;
                        data.IsUserDefined = isUserdefined;
                        data.ValidationDetails = "Checksum digit does not agree with the rest of the input";

                    }
                    
                }

            }
            
            return data;
        }

        int[] GetIntArray(int num)
        {
            List<int> listOfInts = new List<int>();
            while (num > 0)
            {
                listOfInts.Add(num % 10);
                num = num / 10;
            }
            listOfInts.Reverse();
            return listOfInts.ToArray();
        }

       
    }
}
