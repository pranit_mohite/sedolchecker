﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SedolValidation
{
    class Program
    {
        static void Main(string[] args)
        {
            string val;
            // Scenario:**  Null, empty string or string other than 7 characters long
            Console.Write("Enter Sedol: ");
            val = Console.ReadLine();
            ISedolValidator obj = new SedolValidater();
            var result = obj.ValidateSedol(val);
            Console.WriteLine(result.InputString + "|" + result.IsValidSedol + "|" + result.IsUserDefined + "|" + result.ValidationDetails);
            Console.WriteLine("Press enter to close...");
            Console.ReadLine();
        }
    }
}
